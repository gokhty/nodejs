var socket = io.connect("http://localhost:3000", {"forceNew": true});

socket.on("messages", function(data){
	console.log(data);
	render(data);
});

function render(data){
	var html = data.map(function(x, index){
		return(`<div>
					<strong>${x.nombre}</strong>
					<em>${x.texto}</em>
				</div>`);
	}).join(" ");
	document.querySelector("#messages").innerHTML= html;
}

function addMessage(e){
	var payload = {
		id: 2,
		texto :document.querySelector("#texto").value,
		nombre: document.querySelector("#user").value
	};

	socket.emit("new-message", payload);
	return false;
}


/*
var socket = io.connect("http://localhost:3000", {"forceNew": true});

socket.on("messages", function(data){
	console.log(data);
	render(data);
});

function render(data){
	var html = `<div>
					<strong>${data.author}</strong>
					<em>${data.text}</em>
				</div>`;

	document.querySelector("#messages").innerHTML= html;
}

function addMessage(e){
	var payload = {
		author: document.querySelector("#user").value,
		texto: document.querySelector("#texto").value
	};

	socket.emit("new-message", payload);
	return false;
}
*/